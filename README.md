# gwcloud_db_search_helm

## Assumptions
- Helm3 is [installed](https://helm.sh/docs/intro/install/)
- Relevant docker images and container image registry exists
- Relevant vault access and secrets exists
- Relevant storage access and storage solutions exist

## System Prep
- Initialise helm repo by executing `helm create gwcloud_db_search`
- Move helm output by `mv gwcloud_db/* . && mv gwcloud_db_search/.helmignore .`
- Cleanup `rmdir gwcloud_db_helm`

## Chart components
| K8s Resource | Comment |
| --- | --- |
| deployment | Stateful deployment for bilby application. Generates PersistentVolumeClaim(s) |
| role | Access rules to k8s resources |
| rolebinding | k8s construct to assigning role(s) to service account(s) |
| secrets | Placeholder resource populated by init container defined in `deployment` |
| service | k8s construct allowing inter micro service communication |
| serviceaccount | k8s application account used to convert secrets stored in Hashicorp Vault to K8s secrets through an init container defined in `deployment` |
|||

## Chart values
Dynamic variables declared and initialised in [values.yaml](./values.yaml)
| Variable | Default | Comment |
| --- | --- | --- |
| `deployment.name` | gwcloud-db-search | Name value of k8s deployment resource |
| `image.repository` | nexus.gwdc.org.au/docker/gwcloud_db_search | Container image repository |
| `image.tag` | 0.10 | Container image tag |
| `vaultAnnotations.role` | db-search | Role configured at the vault server `vault.gwdc.org.au` |
| `vaultAnnotations.secrets` | [] | list of vault kv engines used as a reference for populating container env variables |

## Architecture
TBA

## Prerequisites
```bash
# Vault dependencies
#   Vault cli required
#   cli must have network access to vault
vault login -address=$VAULT_HTTPS_FQDN -method=github -token=$ACCESS_TOKEN

# Add vault policy
tee db-search.policy.hcl <<EOF
# Read db-search deployment config
path "kv/gwcloud/db-search"
{
  capabilities = ["list", "read"]
}

# Read gwcloud common credentials
path "kv/gwcloud/common"
{
  capabilities = ["list", "read"]
}
EOF
vault policy write db-search db-search.policy.hcl

# Sanity Check
vault policy read db-search

# Add vault kubernetes role
vault write auth/kubernetes/role/db-search \
    bound_service_account_names=db-search \
    bound_service_account_namespaces=gwcloud \
    policies=default,db-search \
    ttl=1h

# Sanity Check
vault read auth/kubernetes/role/db-search
```

## Chart Development
```bash
# Code Linting
helm lint

# Display default helm values
helm show values .

# Render template files with values.yaml as payload
helm template .

# Packaging
#   Chart packaged as $(Chart.name)-$(Chart.version).tgz
rm -rf *.tgz
helm package $(PWD)

# Nexus upload
curl -u "$(NEXUS_ID):$(NEXUS_PASSWORD)" https://nexus.gwdc.org.au/repository/helm/ --upload-file `ls *.tgz` -v
```
## Support
TBA

## ToDo
- [x] Integrate Vault secrets.
- [x] Proof of concept deployment.
- [x] CI for testing the helm chart.
- [ ] CD for deploying packaged charts to `https://nexus.gwdc.org.au/#browse/browse:helm`
- [ ] Runtime tests